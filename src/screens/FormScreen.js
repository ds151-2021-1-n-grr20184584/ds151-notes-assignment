import React, { useContext, useState } from 'react'
import { Button, StyleSheet, TextInput, View } from 'react-native'
import { NotesContext } from '../contexts/NotesContext'

const FormScreen = ({ navigation, route }) => {
  const notesContext = useContext(NotesContext)
  const item = route.params ? route.params.item : { title: '', content: '' }
  const [title, setTitle] = useState(item.title)
  const [content, setContent] = useState(item.content)

  const handleAddNote = () => {
    if (title == '' || content == '') return

    if (item.id) {
      notesContext.updateNote({ id: item.id, title, content });
    } else {
      notesContext.addNote({ title, content });
    }
    setTitle(() => '')
    setContent(() => '')
    navigation.navigate('Home')
  }

  return (
    <View style={styles.container}>
      <TextInput style={styles.input} placeholder="Title" value={title} onChangeText={(text) => setTitle(() => text)} />
      <TextInput style={styles.input} placeholder="Content" value={content} onChangeText={(text) => setContent(() => text)} />
      <Button style={styles.button} title="Add" onPress={() => handleAddNote()} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center'
  },
  input: {
    padding: 10,
    margin: 5,
    borderBottomColor: '#111315'
  },
  button: {
    padding: 5
  }
});

export default FormScreen
