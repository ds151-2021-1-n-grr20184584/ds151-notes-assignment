import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, Button, View, FlatList } from 'react-native';
import NoteItem from '../components/NoteItem';
import { NotesContext } from '../contexts/NotesContext';

const temp = [
  { id: 1, title: 'lorem', content: 'ipsum dolor sit' },
  { id: 2, title: 'lorem', content: 'ipsum dolor sit' },
];

const HomeScreen = ({ navigation }) => {
  const notesContext = useContext(NotesContext)

  return (
    <View style={styles.container}>
      <Button title="Add Note" onPress={() => navigation.navigate('Form')} />
      <FlatList
        data={notesContext.state.notes}
        keyExtractor={({ id }) => id}
        renderItem={({ item }) => <NoteItem item={item} navigation={navigation} />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default HomeScreen;
